CAMERA_PACKAGE_NAME := com.android.camera


ifeq ($(filter davinci raphael cepheus,$(TARGET_DEVICE)),)

# Overlay
PRODUCT_PACKAGES += \
    MiuiCameraOverlay

# Common blobs
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/common/system/lib,$(TARGET_COPY_OUT_SYSTEM)/lib) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/common/system/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/common/vendor/lib,$(TARGET_COPY_OUT_VENDOR)/lib) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/common/vendor/lib64,$(TARGET_COPY_OUT_VENDOR)/lib64) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/common/vendor/bin,$(TARGET_COPY_OUT_VENDOR)/bin) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/common/vendor/etc,$(TARGET_COPY_OUT_VENDOR)/etc) \
    vendor/xiaomi/miuicamera/configs/common/system/lib64/libcamera_algoup_jni.xiaomi.so:$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib/arm64/libcamera_algoup_jni.xiaomi.so

endif

ifeq ($(filter davinci,$(TARGET_DEVICE)),)

# Davinci(in) blobs
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/davinci/vendor/lib,$(TARGET_COPY_OUT_VENDOR)/lib) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/davinci/vendor/lib64,$(TARGET_COPY_OUT_VENDOR)/lib64)

endif

ifeq ($(filter raphael,$(TARGET_DEVICE)),)

# Raphael(in) blobs
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/raphael/vendor/lib,$(TARGET_COPY_OUT_VENDOR)/lib) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/raphael/vendor/lib64,$(TARGET_COPY_OUT_VENDOR)/lib64)

endif

ifeq ($(filter cepheus,$(TARGET_DEVICE)),)

# Cepheus blobs
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/cepheus/vendor/lib,$(TARGET_COPY_OUT_VENDOR)/lib) \
    $(call find-copy-subdir-files,*,vendor/xiaomi/miuicamera/configs/cepheus/vendor/lib64,$(TARGET_COPY_OUT_VENDOR)/lib64)

endif

$(call inherit-product, vendor/xiaomi/miuicamera/camera-vendor.mk)
